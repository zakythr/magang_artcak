@include('layouts.utils.errorMessages')

<div class="form-group row">
    {{ Form::label('name', 'Nama', ['class' => 'col-sm-4 col-form-label form-label--required']) }}
    <div class="col-sm-8">
        {{ Form::text('name', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('display_name', 'Tampilan Nama', ['class' => 'col-sm-4 col-form-label']) }}
    <div class="col-sm-8">
        {{ Form::text('display_name', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group row">
    {{ Form::label('description', 'Deskripsi', ['class' => 'col-sm-4 col-form-label']) }}
    <div class="col-sm-8">
        {{ Form::text('description', null, ['class' => 'form-control']) }}
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-4">
        <div class="form-label--required">Hak Akses</div>
        <small class="form-text text-muted">Tandai hak akses yang ingin diberikan pada peran.</small>
    </div>
    <div class="col-sm-8">
        @forelse ($permissions as $permission)
            <div class="form-check">
                <label for="{{ $permission->name }}" class="form-check-label">
                    {{ Form::checkbox('permissions[]', $permission->id, null, ['class' => 'form-check-input']) }}
                    {{ ucfirst($permission->name) }}
                </label>
            </div>
        @empty
            <div class="form-text text-muted">Tidak ada hak akses yang tersedia.</div>
        @endforelse
    </div>
</div>