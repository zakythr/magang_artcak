<?php

namespace App\Http\Controllers\Api\V1\Autentikasi;

use Illuminate\Http\Request;
use App\Http\Controllers\Api\ApiController;
use App\User;
use Hash;

class AutentikasiController extends ApiController
{
    public function login(Request $request)
    {
        return $this->guardWithValidation($request, [
            'username' => 'required|string',
            'password' => 'required|string',
        ], [], function() use ($request) {
            $user = User::where('username', $request->username);
            if ($user->count() > 0) {
                $user = $user->first();
                if (Hash::check($request->password, $user->password)) {
                    $token = hash('sha256', $user->id.date('U').config('app.key'));
                    $user->token_login_api = $token;
                    $user->save();
                    $result = [
                        'token' => $token,
                    ];
                    return $this->success('Login sukses.', $result);
                } else {
                    return $this->failure('Password tidak sesuai.');
                }
            } else {
                return $this->notFound('User tidak ditemukan.');
            }
        });
    }

    public function logout(Request $request)
    {
        $user = $request->user();
        $user->token_login_api = NULL;
        $user->save();
        return $this->success('Logout sukses.');
    }
}
