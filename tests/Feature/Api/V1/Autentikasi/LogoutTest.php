<?php

namespace Tests\Feature\Api\V1\Autentikasi;

use Tests\Feature\Api\V1\EncapsulatedApiTestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use DB;

class LogoutTest extends EncapsulatedApiTestCase
{
    use DatabaseTransactions;

    protected function setUp()
    {
        parent::setUp();
        $this->addUrls([
            'logout' => '/api/v1/autentikasi/logout',
        ]);
    }

    public function testLogout()
    {
        $loginData = $this->loginUser();
        $this->assertTrue($loginData['user']->token_login_api !== NULL);
        $response = $this->json('POST', $this->urls['logout'], [
            'token' => $loginData['token'],
        ]);
        $updatedUser = DB::table('users')->where('id', $loginData['user']->id)->first();
        $this->returnSuccess($response, 'Logout sukses.');
        $this->assertTrue($updatedUser->token_login_api === NULL);
    }

    public function testLogoutWithoutToken()
    {
        $response = $this->json('POST', $this->urls['logout'], []);
        $this->returnUnauthorized($response);
    }

    public function testLogoutUsingInvalidToken()
    {
        $loginData = $this->loginUser();
        $this->assertTrue($loginData['user']->token_login_api !== NULL);
        $response = $this->json('POST', $this->urls['logout'], [
            'token' => 'invalid'.$loginData['token'],
        ]);
        $updatedUser = DB::table('users')->where('id', $loginData['user']->id)->first();
        $this->returnUnauthorized($response);
        $this->assertTrue($updatedUser->token_login_api == $loginData['token']);
    }
}